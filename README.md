# webgis-house-selection

#### 介绍
本科毕业设计：基于WebGIS的购房选址空间决策系统设计。

#### 软件架构
服务器：Flask轻量级服务器
前端：Vue + Element Plus + OpenLayers


#### 安装教程
1. 数据层
Scrapy爬虫（数据来源：©贝壳网）

2.  服务端
pip install
启动server.py

3.  前端
npm install
启动前端Server： npm run server


#### 使用说明
1. 武汉市房源列表页面（738_20条数据）
![武汉市房源列表页面（738_20条数据）](https://images.gitee.com/uploads/images/2021/0528/220222_21020811_8602228.png "武汉市房源列表页面（738_20条数据）.png")

2. 房源信息条件查询
![房源信息条件查询](https://images.gitee.com/uploads/images/2021/0528/220049_7dbace65_8602228.png "房源信息条件查询.png")

3. 房源信息关键字模糊查询
![房源信息关键字模糊查询](https://images.gitee.com/uploads/images/2021/0528/220009_a5fb3776_8602228.png "房源信息关键字模糊查询.png")

4. 地图图层管理面板
![地图图层管理面板](https://images.gitee.com/uploads/images/2021/0528/215553_6ce4e66e_8602228.png "地图图层管理面板.png")

5. 矢量边界分布（武汉市）
![矢量边界分布（武汉市）](https://images.gitee.com/uploads/images/2021/0528/220159_2f26767e_8602228.png "矢量边界分布（武汉市）.png")

6. 房源分布图（武汉市）
![房源分布图（武汉市）](https://images.gitee.com/uploads/images/2021/0528/215814_f30e73d6_8602228.png "房源分布图（武汉市）.png")

7. 房源热点分布（武汉市）
![房源热点分布（武汉市）](https://images.gitee.com/uploads/images/2021/0528/215925_c8c375f4_8602228.png "房源热点分布（武汉市）.png")


#### 参与贡献

1.  ©awang：wzdong.26@qq.com