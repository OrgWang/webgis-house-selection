import axios from "axios"

export function getBublelist(params) {
  return axios({
    url: 'http://localhost:8083/bublelist',
    method: 'get',
    params: params
  })
}