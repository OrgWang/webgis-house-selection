import axios from "axios"

export function getAddress(code) {
  return axios({
    url: 'http://localhost:8083/addressSelection',
    method: 'get',
    params: {
      code: code
    }
  })
}