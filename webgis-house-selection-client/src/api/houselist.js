import axios from "axios"

export function getHouselist(params) {
  return axios({
    url: 'http://localhost:8083/houselist',
    method: 'get',
    params: params
  })
}